#
# Number manipulation 
#
num1 = 12
num2 = 18

# Addition
print(num1 + num2) # Output: 30

# Subtraction
print(num1 - num2) # Output: -6

# Multiplication
print(num1 * num2) # Output: 216

# Division
print(num1 / num2) # Output: 0

print(float(num1 / num2)) # Ouptut: 0.0
print(num1 / float(num2)) # Output: 0.666666666667
print(float(num1) / num2) # Output: 0.666666666667

# Modulus
print(num1 % num2) # Output: 12

#
# String manipulation
#
firstName = "Saroj"
lastName = "Rana"
print(firstName + lastName) # Output: SarojRana
print(firstName + "" + lastName) # Output: Saroj Rana

# Repeat string multiple times
print("hey" * 5) # Output: heyheyheyheyhey

sentence = "Hey, I am learning."
# Get "I" from sentence
print(sentence[5]) # Output: I

# Get "Hey" from sentence
print(sentence[0:3]) # Output: Hey

# Get first 3 characters from sentence
print(sentence[:3]) # Output: Hey

# Get sentence trimming 4 characters at the end
print(sentence[:-4]) # Output: Hey, I am learn
