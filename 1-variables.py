# Single assignment
age = 25
sentence = "Hey, I'm Saroj."

print(age) # output: 25
print(sentence) # output: Hey, I'm Saroj.

# Multiple assignment
name, address, postal = "Gorkhe", "Gorkha", 34000 

print("Name = " + name) # output: Name = Gorkhe
print("Address = " + address) # output: Address = Gorkha

# Type conversion - converting integer to string
print("Postal code = " + str(postal)) # output: Postal code = 34000
