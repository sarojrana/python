# Python Exercises

| S.N. | Description             | Source Code                       |
| ---- | ----------------------- | --------------------------------- |
| 1    | Variables               | [Link](1-variables.py)            |
| 2    | Arithmetic Operators    | [Link](2-arithmetic-operators.py) |
| 3    | Placeholders in Strings | [Link](3-string-placeholder.py)   |
