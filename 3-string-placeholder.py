#
# Placeholders in Strings
#

name = "Gorkhe"
age = 15
distance = 3550.5

# Old method
# %s for string value
# %d for integer value
# %f for float value and .2 limits to two decimal places 
sentence = "%s is %d years old. He travelled %.2f kilometers."

print(sentence%(name, age, distance)) # Output: Gorkhe is 15 years old. He travelled 3550.50 kilometers.

#
# New method
#
sentence = "{} is {} years old. He travelled {} kilometers.".format(name, age, distance)
print(sentence) # Output: Gorkhe is 15 years old. He travelled 3550.5 kilometers.

sentence = "{0} is {1} years old. He travelled {2} kilometers. Bravo! {0}".format(name, age, distance)
print(sentence) # Output: Gorkhe is 15 years old. He travelled 3550.5 kilometers. Bravo! Gorkhe

sentence = "{name} is {age} years old. He travelled {distance} kilometers.".format(age=age, name=name, distance=distance)
print(sentence) # Output: Gorkhe is 15 years old. He travelled 3550.5 kilometers.
